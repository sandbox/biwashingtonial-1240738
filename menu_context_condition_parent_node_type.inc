<?php
// $Id: context_condition_menu.inc,v 1.1.2.5 2010/07/30 06:28:43 yhahn Exp $

/**
 * Expose menu items as a context condition.
 */
class menu_context_condition_parent_node_type extends context_condition {
  /**
   * Override of condition_values().
   */
  function condition_values() {
    $values = array();
    foreach (node_get_types() as $type) {
      $values[$type->type] = check_plain($type->name);
    }
    return $values;
  }

  /**
   * Override of condition_form().
   * Use multiselect checkboxes widget.
   */
  function condition_form($context) {
    $form = parent::condition_form($context);
    $form['#type'] = 'select';
    $form['#multiple'] = TRUE;
    $form['#type'] = 'checkboxes';
    return $form;
  }

  /**
   * Override of execute().
   */
  function execute() {
    if ($this->condition_used()) {
      $parent_menu_link = context_get('menu_context', 'parent_menu_link');
      if (is_object($parent_menu_link) && $parent_menu_link->router_path == 'node/%') {
        $parent_node = node_load( (int)substr($parent_menu_link->link_path, 5) );
        foreach ($this->get_contexts($parent_node->type) as $context) {
          $this->condition_met($context, $parent_node->type);
        }
      }
    }
  }
}
